'use strict';

// inspired by https://blog.risingstack.com/getting-started-with-nodejs-and-mqtt/

var config = require('./config');
var mqtt = require('mqtt');
var status = require('status');
var client = mqtt.connect({
	host: config.host,
	port: config.port,
	protocol: (parseInt(config.port) === 8883 ? 'mqtts' : 'mqtt'),
	username: config.username,
	password: config.password,
	connectTimeout: 60 * 1000,
	keepalive: 3600
});

var feed = "micro0301";
var connected = false;
var updateInterval = 2000;

var connectedStream = `${config.username}/feeds/micro0301_connected`;
var cpuStream = `${config.username}/feeds/micro0301_cpu0`;
var messageStream = `${config.username}/feeds/micro0301_messages`;

client.on('connect', () => {
   client.subscribe(connectedStream);
   client.subscribe(cpuStream);
   client.subscribe(messageStream);
   connected = true;
   client.publish(connectedStream,'True');
   console.log('connected');
});

client.on('message', function (topic, message) {
  console.log('RECD ' + topic.toString() + " - " + message.toString());
});

setInterval(sendStatusUpdate,updateInterval);

function sendStatusUpdate() {
   var temp = "";
   var exec = require('child_process').exec;
   var cmd = 'cat /sys/class/thermal/thermal_zone0/temp';
   exec(cmd, function(error, stdout, stderr) {
      temp = Math.round(stdout / 1000).toString();
      client.publish(cpuStream,temp);
   });
}

function handleAppExit (options, err) {  
  if (err) {
    console.log(err.stack)
  }

  if (options.cleanup) {
    client.publish(connectedStream, 'False')
  }

  if (options.exit) {
    process.exit()
  }
}

/**
 * Handle the different ways an application can shutdown
 */
process.on('exit', handleAppExit.bind(null, {  
  cleanup: true
}))
process.on('SIGINT', handleAppExit.bind(null, {  
  exit: true
}))
process.on('uncaughtException', handleAppExit.bind(null, {  
  exit: true
}))
